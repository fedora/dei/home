# Home - Issues and Discussions



# Fedora Diversity, equity, and inclusion

The goal of our team is to encourage diversity and inclusion in the Fedora Community. By inclusion, we mean to create a comfortable environment to encourage and empower everyone to be part of the community. Inclusion is not about just being respectful to each other; it is to make the community accessible to groups from under-represented, disabled and many other communities. Our [DEI Resource page](https://docs.fedoraproject.org/en-US/diversity-inclusion/resources/resources/) has tons of valuable information for someone looking to learn more on the topic.

## What is this place?

The Fedora DEI Team uses this repository to record ongoing work and to track issues which need a specific resolution.
Tickets are normally discussed during weekly meetings.

You can read more about the Fedora DEI Team on the [DEI's docs' page](https://docs.fedoraproject.org/en-US/diversity-inclusion/)

## File a new ticket

To file a new ticket, open a [new issue](https://gitlab.com/fedora/dei/home-issues-and-discussions/-/issues).

## How to contact the DEI Team

Other than this repository, you can also find the members of the Fedora DEI Team in the following places:

* [Discourse forum](https://discussion.fedoraproject.org/tag/dei)
* [Element/Matrix](https://app.element.io/#/room/#fedora-diversity:matrix.org)



