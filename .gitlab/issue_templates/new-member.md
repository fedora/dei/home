<!-- This template is for Fedora DEI team members to nominate a new member to join the team.-->

Per our [nomination process](https://docs.fedoraproject.org/en-US/dei/get-involved/#sponsorship), I am opening a nomination ticket for **NAME** to join the Fedora DEI Team.

* **Name**:
* **FAS username**:
* **Introduction post link from the Fedora Discussion thread**: <!--From the introduction thread here: https://discussion.fedoraproject.org/t/dei-team-introductions-say-hi-and-get-to-know-each-other/37860 -->
* **Involvement in Fedora and/or DEI group**: <!--Explain why the nominated person should join the team. See [Example 1](https://gitlab.com/fedora/dei/home/-/issues/27) and [example 2](https://gitlab.com/fedora/dei/home/-/issues/24).-->

I am seeking at least three other +1's to confirm this sponsorship.

<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"category::onboarding" ~"scope::new" ~"?::needs team vote"

